$(document).ready(function () {

    // renvoie la concaténation des "tranches" converties
    function convertNumber(number){
        


        return number;
    }

    // cette fonction permet l'affichage du nombre converti
    function showNumber(number){
        $("#window").removeClass("red");
        $("#window").addClass("black");
        let stringNumber = convertNumber(number);
        $("#window").html(stringNumber);
    }

    // cette fonction permet l'affichage du message d'erreur en rouge
    function showError(){
        $("#window").removeClass("black");
        $("#window").addClass("red");
        $("#window").html("ERREUR : Veuillez choisir un nombre entr 0 et 1000");
    }

    // au clic on récupère le nombre a convertir
   $("#numberButton").click(function(){
       let form = $("#numberForm").serialize();
       let value = form.split("=");
       //console.log(form);

       if(value[1] <= 1000 && value[1] >= 0){
            showNumber(value[1]);
       } else {
            showError();
       }
   });
});