<?php require('include/header.php'); ?>
    <h1 class="textCentred">Premiers pas en JS</h1>
    
    <section class="init row">
        <article class="var col-12 col-md-6">
            <h2>Les variables</h2>
            <ul>
                <li>Déclaration: <br>let, var, const permettent d'initialiser une variable. La différence vient de la portée.</li>
                <li>Typage: int, string, array. Il s'agit des principaux types de variables. Il faut y faire attention quand ils'agit de les manipuler. Par exemple, string "1" + string "1" == "11" alors que int 1 + int 1 == 2</li>
            </ul>
            <h2>Les fonctions</h2>
            <ul>
                <li>Déclaration: <br>
                function(arguments){} <br>
                (arguments) => {}</li>
                <li>return : renvoie une valeur, attention, stoppe le la fonction à ce niveau</li>
            </ul>
            <h2>Les tableaux</h2>
            <ul>
                <li>Déclaration: <br>
                let myArray = ["entree1", "case2"]; <br>
                myArray[1] : "case2" <br>
                let assosArray = { key: "value"} <br>
                assosArray["key"] || assosArray.key : "value"
                </li>
                <li>Quelques méthodes : push(), ajoute une case au tableau / string.split() découpe un string en tableau / array.join() fusione un tableau en string</li>
            </ul>
            <h2>Les boucles</h2>
            <ul>
                <li>for(compteur; condition; action fin de boucle){}</li>
                <li>for(let variable in iterable){} : équivalent à foreach</li>
            </ul>
        </article>

        <article class="function col-12 col-md-6">
        <h2>Les conditions</h2>
            <ul>
                <li>A la base des conditions, les booléen. True/False, cela peut être le résultat d'une fonction ou une variable. Envie de tester?
                <p class="blockCode">alert(1+1 == 3);</p>
                </li>
                <li>Les opérateurs de comparaison:
                    <img src="assets/pictures/operator.jpg" alt="Operateurs">
                </li>
                <li>Les opérateurs logiques: pour enchainer les comparaisons. <br>
                    || => ou ; && => et
                </li>
                <li>Les structures conditionnelles:
                    <ul>
                        <li>if / elsif / else => si / sinon si /  <br>
                            if(condition){ <br>
                            <span class="marge"></span>code <br>
                            } else if(sinon condition2){ <br>
                            <span class="marge"></span>code <br>
                            } else { <br>
                            <span class="marge"></span>code si aucune des conditions n'est remplie <br>
                            } <br>
                        </li>
                        <li>switch, idéal pour enchainer les conditions tout en gardant le code joli. <br>
                            Deux possibilités: <br>
                        </li>
                        switch(variable){ || switch(true){<br>
                            <span class="marge"></span>case "possibilité 1": || case condition:<br>
                            <span class="marge2"></span> code <br>
                            <span class="marge"></span>break; <br>
                            
                            <span class="marge"></span>default: <br>
                            <span class="marge2"></span> code <br>
                            <span class="marge"></span>break; <br>
                        } <br>
                    </ul>
                </li>
            </ul>
        </article>
    </section>
    
    <div id="window"></div>

<?php require('include/footer.php'); ?>  