<?php require('include/header.php'); ?>
    <h1 class="textCentred">TP Convertir les nombres en lettres</h1>
    <p class="textCentred">Le but de l'exercice est de convertir en lettre un nombre compris entre 0 et 1000.</p>
    
    <section class="init row">
        <form id="numberForm" class="col-5 col-md-10">
            <input type="number" name="number" id="number">
            <button id="numberButton" type="button" class="btn btn-secondary btn-lg">Convertir</button>
            <span class="marge" id="window"></span>
        </form>
    </section>
    

<?php require('include/footer.php'); ?>  